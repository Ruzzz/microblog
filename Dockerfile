FROM python:3.7

WORKDIR /app/mblog
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
COPY . .
EXPOSE 8080
CMD [ "python", "./mblog_api.py" ]
